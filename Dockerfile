FROM openjdk:8-jdk-alpine
WORKDIR /App
COPY target/Api-Investimentos-0.0.1-SNAPSHOT.jar .
CMD ["java","-jar", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]
